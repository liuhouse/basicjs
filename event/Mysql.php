<?php
header('Content-type:text/html;charset=utf8');
$config = [
    'host' => 'localhost',
    'user' => 'root',
    'password' => 'root',
    'database' => 'lh_fresh',
    'charset' => 'utf8'
];

try {
    $dns = sprintf('mysql:host=%s;dbname=%s;charset=%s',$config['host'],$config['database'],$config['charset']);
    $pdo = new PDO($dns,$config['user'],$config['password']);
    return $pdo;
}catch(PDOException $e){
    die($e->getMessage());
}