//左边的分类处理
class HD{
    constructor(el) {
        this.el = el;
        this.one_class = document.querySelector('.one_class');
        this.two_class = document.querySelector('.two_class');
        //准备dt点击事件
        this.el.addEventListener('click',this.handle.bind(this));
        //准备鼠标滑过事件
        this.el.addEventListener('mouseover',this.over.bind(this));
        this.main = new Main(document.querySelector('ul'));
        this.ul = document.querySelector('ul');
        this.alertMessage = document.querySelector('#alertMessage');
        this.ul.innerHTML = '<h2>欢迎来到后台仓库管理系统...</h2>';
    }
    //准备点击事件
    handle(e){
        const action = e.target.dataset.action;
        this.color = e.target.dataset.color;
        let dt_value;
        let dd_value;
        if(e.target.tagName == 'DT'){
                dt_value = e.target.innerHTML;
                dd_value = e.target.nextElementSibling.innerHTML;
        }else{
                dt_value = e.target.parentElement.firstElementChild.innerHTML;
                dd_value = e.target.innerHTML;
        }
        this.one_class.innerHTML = dt_value;
        this.two_class.innerHTML = dd_value;
        this[action](e);
    }

    //准备鼠标滑过事件
    over(e){
        let _target = e.target;
        if(_target.tagName == 'DD'){
            let dd_value = _target.innerHTML;
            let parent_target = _target.parentElement;
            let dt_value = parent_target.firstElementChild.innerHTML;
            this.one_class.innerHTML = dt_value;
            this.two_class.innerHTML = dd_value;
        }
    }

    //切换
    toggle(e){
        //获取属性值
        let hidden_value = e.target.nextElementSibling.hidden;
        this.clearActive(e);
        e.target.nextElementSibling.style.color = '#eb4d4b';
        if(hidden_value){
            hidden_value = false;
            let class_id = e.target.nextElementSibling.dataset.id;
            this.alertMessage.hidden = false;
            this.requestUrl(class_id);
        }else{
            hidden_value = true;
        }
        let dds = e.target.parentElement.children;
        //删除数组的第一个元素
        let dds_arr = [...dds].filter((els,index) => (els.dataset.action == 'active'));
        dds_arr.forEach((es,is) => {
            es.hidden = hidden_value;
        });
    }

    active(e){
        this.clearActive(e);
        e.target.style.color = this.color;
        let class_id = e.target.dataset.id;
        this.alertMessage.hidden = false;
        this.requestUrl(class_id);
    }

    requestUrl(class_id){
        this.ul.innerHTML = '';
        HTTP.get(`good.php?class_id=${class_id}`,{
            responseType : 'text'
        }).then((response) => {
           let res = response;
           this.alertMessage.hidden = true;
           if(res != 'no'){
                res = JSON.parse(response);
                res.forEach(element => {
                    this.main.createHtml(`https://www.xiaobangcai.com/${element.cover}`,`${element.trade_name}`);
                });
           }else{
               this.ul.innerHTML = '<h2>此分类下暂无数据....</h2>';
           }
        });
    }


    clearActive(e){
        [...document.getElementsByTagName('dd')].forEach((el,index) => {
            el.style.color = '#ffffff';
        });
    }

}

//右边的主内容处理
class Main{
    constructor(el){
        this.el = el;
    }
    createHtml(src,txt){
        let li = document.createElement('li');
        let img = document.createElement('img');
        let a = document.createElement('a');
        img.src = src;
        a.href = '#';
        a.innerHTML = txt;
        li.appendChild(img);
        li.appendChild(a);
        this.el.appendChild(li);
    }

}

new HD(document.querySelector('.tab'));




