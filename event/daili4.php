<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tab栏的切换</title>
    <link rel="stylesheet" href="daili4.css">
</head>
<body>
<?php
$pdo = require_once 'Mysql.php';
$query = $pdo->query("SELECT * FROM bd_private_cate WHERE supplier_id = 67 AND private_class_belong = 0");
$class_list = $query->fetchAll(PDO::FETCH_ASSOC);

?>
    <div class="tab">
        <?php
            foreach ($class_list as &$val){
        ?>
        <dl>
            <dt data-action="toggle"><?php echo $val['private_class_name'];?></dt>
            <?php
                $two_sql = "SELECT * FROM bd_private_cate WHERE supplier_id=:supplier_id AND private_class_belong=:private_class_belong";
                $sth = $pdo->prepare($two_sql);
                $sth->execute(['supplier_id'=>67,'private_class_belong'=>$val['private_one_class']]);
                $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
                foreach ($rows as &$v){
            ?>
                    <dd data-action="active" data-color="#eb4d4b" data-id=<?php echo $v['private_one_class'] ?> hidden="true"><?php echo $v['private_class_name']; ?></dd>
                <?php
                    }
                ?>
        </dl>
        <?php
            }
        ?>

    </div>
    <div class="current_info">当前位置：<span class="one_class">后端语言</span> <span class="bj">></span> <span class="two_class">php</span></div>
    <div id="main">
        <ul>
            
        </ul>
    </div>

    <div id="alertMessage" hidden="true">加载中...</div>

    <script type="text/javascript" src="http.js"></script>
    <script type="text/javascript" src="daili4.js"></script>
    <script>
        
    </script>
</body>
</html>