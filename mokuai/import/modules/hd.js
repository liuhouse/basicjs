// export const site = "后盾人";

// export const func = function(){
//     return "is a module function";
// };

// export class User{
//      show(){
//         console.log("user.show");
//     }
// }



// const site = "后盾人";

// const func = function(){
//     return "is a module function";
// };

// class User{
//      show(){
//         console.log("user.show");
//     }
// }

// //指量导出
// // export {site,func,User};

// //导出别名
// export {site,func as action,User as user};


//单一导出
// export default class{
//     static show(){
//         console.log("User.method");
//     }
// }


//从程序来说如果将一个导出命名为default也算默认导出
// class User{
//     static show(){
//         console.log('User.method');
//     }
// }

// export {User as default};


 //混合导出
//  const site = "后盾人";
//  const func = function(){
//      console.log("is a module function");
//  }

//  export default class{
//      static show(){
//          console.log("user.show");
//      }
//  }

//  export {site,func};



//也可以使用下面扥方式导出模块
const site = "后盾人";
const func = function(){
    console.log("is a module function");
};
class User{
    static show(){
        console.log('User.show');
    }
}

export {site,func,User as default};





