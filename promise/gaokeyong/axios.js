//上例中的处理是在队列中完成的,不方便定制业务，下面将promise处理剥离到外部
//后台请求处理类
export default function (url) {
    return new Promise((resolve,reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET',url);
        xhr.send();
        xhr.onload = function () {
            if(this.status === 200){
                resolve(this.response);
            }else{
                reject(this);
            }
        }
    })
}