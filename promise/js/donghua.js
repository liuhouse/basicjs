//动画
    //隐藏  显示
    class Animation{
        constructor(){

        }
        //设置元素的隐藏
        hide(){
            this.style.display = 'none';    
        }

        //设置元素的显示
        show(){
            this.style.display = 'block';
        }

        //设置鼠标在元素上面
        mousemove(el){
            el.onmousemove = function(){
                this.style.opacity = 1;
            }
        }

        //设置鼠标离开元素
        mouseleave(el){
            el.onmouseleave = function(){
                this.style.opacity = 0.8;
            }
        }

        //点击事件
        click(el,color='',zhezhao='',success=''){
            el.onclick = () => {
                const zhezhaoS = document.getElementById(zhezhao);
                const successS = document.getElementById(success);
                const select_color = color.substr(1);
                const input = document.getElementById(select_color);
                input.select();
                document.execCommand('copy');
                zhezhaoS.style.display = 'block';
                zhezhaoS.style.backgroundColor = color;
                successS.style.backgroundColor = color;
                successS.style.fontSize = '3em';
                successS.innerHTML = '复制成功LH ' + color;
                setTimeout(() => {
                    zhezhaoS.style.display = 'none';
                    successS.style.fontSize = '1.3em';
                },600);
            }
            
        }
    }

   /**
    *创建表单元素
    *ElemName 元素名字
    *type     元素类型
    *classList 元素的类
    *color   元素的颜色
   */
    class CreateFormElem extends Animation{
        constructor(ElemName,type='text',classList='',color=''){
            super();
            this.ElemName = ElemName;
            this.type = type;
            this.classList = classList;
            this.color = color;
        }

        //创建输入框
        createInput(){
            let input = document.createElement(this.ElemName);
            input.type = this.type;
            input.classList = this.classList;
            input.setAttribute('value',this.color);
            let id_color = this.color.substr(1);
            input.setAttribute('id',id_color);
            document.body.appendChild(input);
            return input;
        }

        //创建button元素
        createButton(){
            let button = document.createElement(this.ElemName);
            button.type = this.type;
            button.classList = this.classList;
            let buttonText = document.createTextNode(this.color);
            button.appendChild(buttonText);
            button.style.backgroundColor = this.color;
            document.body.appendChild(button);
            return button;
        }

        //创建遮罩对象
        createZZ(){
            let div = document.createElement(this.ElemName);
            div.setAttribute('id','zhezhao');
            //创建弹窗对象
            let div_tan = document.createElement(this.ElemName);
            div_tan.setAttribute('id','success');
            div.appendChild(div_tan);
            document.body.appendChild(div);
            return div;
        }

    }

    let color_arr = [
                 '#fc5c65','#fd9644','#fed330','#26de81','#2bcbba'
                 ,'#eb3b5a','#fa8231','#f7b731','#20bf6b','#0fb9b1'
                 ,'#45aaf2','#4b7bec','#a55eea','#d1d8e0','#778ca3'
                 ,'#2d98da','#8854d0','#a5b1c2','#4b6584','#FD7272'
                ];

    let ZZ = new CreateFormElem("div");
    ZZ.createZZ();
    //创建input
    for(let i = 0 ; i <= 19 ; i++){
        let inputS = new CreateFormElem('input','text','classLH',color_arr[i]);
        let Buttons = new CreateFormElem('button','button','button_class',color_arr[i]);
        inputS.createInput();
        let _button = Buttons.createButton();
        Buttons.mousemove(_button);
        Buttons.mouseleave(_button);
        Buttons.click(_button,color_arr[i],'zhezhao','success');
    }

 