var EventUtil = {
    /*
     *elem 要操作的元素
     *type 事件名称
     *handler 事件处理程序函数
    */
    addHandler : function(element,type,handler){
        if(element.addEventListener){
            element.addEventListener(type,handler,false);
        }else if(element.attachEvent){
            element.attachEvent("on"+type,handler);
        }else{
            //p.onclick = function(){}
            element["on"+type] = handler;
        }
    },
    /**
     * 移除事件对象
    */
    removeHandler : function(element,type,handler){
        if(element.removeEventListener){
            element.removeEventListener(type,handler,false);
        }else if(element.detachEvent){
            element.detachEvent("on"+type,handler);
        }else{
            element["on"+type] = null;
        }
    },
    //获取事件对象
    getEvent:function(event){
        return event ? event : window.event;
    },
    //获取当前操作对象的目标
    getTarget:function(event){
        return event.target || event.srcElement;
    },
    //阻止事假对象
    preventDefault:function(event){
        if(event.preventDefault){
            event.preventDefault();
        }else{
            event.returnValue = false;
        }
    },
    //阻止事件冒泡
    stopPropagation:function(event){
        if(event.stopPropagation){
            event.stopPropagation();
        }else{
            event.cancelBubble = true;
        }
    }

}